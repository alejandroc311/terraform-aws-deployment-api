resource "aws_ecs_cluster" "ecs_cluster" {
  name = "${local.prefix}-ecs-cluster"
  tags = local.common_tags
}

resource "aws_iam_policy" "ecs_cluster_start_task_execution_role_policy" {
  name        = "${local.prefix}-ecs-cluster-start-task-execution-role-policy"
  description = "Policy for retrieving ECR images and writing to logs in order to start task execution"
  path        = "/"
  policy      = file("./template/cluster/task-exec-role.json")
}

resource "aws_iam_role" "ecs_cluster_start_task_execution_role" {
  name               = "${local.prefix}-ecs-cluster-start-task-execution-role"
  assume_role_policy = file("./template/cluster/assume-role-policy.json")
  tags               = local.common_tags
}
resource "aws_iam_role_policy_attachment" "ecs_cluster_start_task_execution_role_policy_attachment" {
  role       = aws_iam_role.ecs_cluster_start_task_execution_role.name
  policy_arn = aws_iam_policy.ecs_cluster_start_task_execution_role_policy.arn
}
resource "aws_iam_role" "ecs_cluster_runtime_task_role" {
  name               = "${local.prefix}-ecs-cluster-runtime-task-role"
  assume_role_policy = file("./template/cluster/assume-role-policy.json")
  tags               = local.common_tags
}
resource "aws_cloudwatch_log_group" "ecs_cluster_tasks_log_group" {
  name = "${local.prefix}-ecs-cluster-tasks-log-group"
  tags = local.common_tags
}

data "template_file" "ecs_containers_definitions" {
  template = file("./template/cluster/container-definitions.json.tpl")
  vars = {
    webapp_image     = var.ecr_image_webapp
    proxy_image      = var.ecr_image_proxy
    log_group_name   = aws_cloudwatch_log_group.ecs_cluster_tasks_log_group.name
    log_group_region = data.aws_region.current.name
    allowed_hosts    = aws_lb.lb.dns_name
    api_url          = var.api_url
    lb_url           = var.lb_host
  }
}
resource "aws_ecs_task_definition" "ecs_cluster_task_definition" {
  family                   = "${local.prefix}-tapd"
  container_definitions    = data.template_file.ecs_containers_definitions.rendered
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  network_mode             = "awsvpc"
  execution_role_arn       = aws_iam_role.ecs_cluster_start_task_execution_role.arn
  task_role_arn            = aws_iam_role.ecs_cluster_runtime_task_role.arn
  tags                     = local.common_tags
}

resource "aws_security_group" "ecs_cluster_security_group" {
  name        = "${local.prefix}-ecs-cluster-security-group"
  description = "Security group for ECS cluster"
  vpc_id      = aws_vpc.vpc.id
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.private_subnet_a.cidr_block, aws_subnet.private_subnet_b.cidr_block]
  }
  ingress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    security_groups = [aws_security_group.lb_sg.id]
  }
  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.lb_sg.id]
  }

  tags = local.common_tags
}
resource "aws_ecs_service" "tapd_ecs_service" {
  name             = "${local.prefix}-tapd-ecs-service"
  cluster          = aws_ecs_cluster.ecs_cluster.name
  task_definition  = aws_ecs_task_definition.ecs_cluster_task_definition.family
  desired_count    = 1
  platform_version = "1.4.0"
  launch_type      = "FARGATE"
  network_configuration {
    subnets         = [aws_subnet.private_subnet_a.id, aws_subnet.private_subnet_b.id]
    security_groups = [aws_security_group.ecs_cluster_security_group.id]
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.lb_target_group_app.arn
    container_name   = "web-app"
    container_port   = 80
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.lb_target_group_proxy.arn
    container_name   = "proxy"
    container_port   = 8080
  }
  tags = local.common_tags
}