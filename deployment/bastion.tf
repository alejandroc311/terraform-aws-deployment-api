data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}
resource "aws_iam_role" "bastion_iam_role" {
  name               = "${local.prefix}-bastion-iam-role"
  assume_role_policy = file("./template/bastion/instance-profile-policy.json")
  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-bastion-iam-role" })
  )
}
resource "aws_iam_role_policy_attachment" "bastion_iam_role_policy_attachment" {
  role       = aws_iam_role.bastion_iam_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}
resource "aws_iam_instance_profile" "bastion_iam_instance_profile" {
  name = "${local.prefix}-bastion-iam-instance-profile"
  role = aws_iam_role.bastion_iam_role.name
}
resource "aws_instance" "bastion" {
  ami                  = data.aws_ami.amazon_linux.id
  instance_type        = "t2.micro"
  user_data            = file("./template/bastion/user-data.sh")
  iam_instance_profile = aws_iam_instance_profile.bastion_iam_instance_profile.name
  key_name             = var.bastion_key_name
  subnet_id            = aws_subnet.public_subnet_a.id
  vpc_security_group_ids = [
    aws_security_group.bastion_sg.id
  ]
  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-bastion" })
  )
}
resource "aws_security_group" "bastion_sg" {
  description = "Control inbound and outbound access to the bastion host"
  name        = "${local.prefix}-bastion-sg"
  vpc_id      = aws_vpc.vpc.id
  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    protocol  = "tcp"
    from_port = 3306
    to_port   = 3306
    cidr_blocks = [
      aws_subnet.private_subnet_a.cidr_block,
      aws_subnet.private_subnet_b.cidr_block,
    ]
  }
  tags = local.common_tags
}