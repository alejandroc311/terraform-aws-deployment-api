resource "aws_lb" "lb" {
  name               = "${local.prefix}-main-lb"
  load_balancer_type = "application"
  subnets = [
    aws_subnet.public_subnet_a.id,
    aws_subnet.public_subnet_b.id
  ]
  security_groups = [
    aws_security_group.lb_sg.id
  ]
  tags = local.common_tags
}

resource "aws_lb_target_group" "lb_target_group_app" {
  name        = "${local.prefix}-app-tg"
  protocol    = "HTTP"
  vpc_id      = aws_vpc.vpc.id
  target_type = "ip"
  port        = 80
  health_check {
    path = "/login"
  }
}
resource "aws_lb_target_group" "lb_target_group_proxy" {
  name        = "${local.prefix}-proxy-tg"
  protocol    = "HTTP"
  vpc_id      = aws_vpc.vpc.id
  target_type = "ip"
  port        = 8080
}
resource "aws_lb_listener" "lb_listener" {
  load_balancer_arn = aws_lb.lb.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_target_group_app.arn
  }
}
resource "aws_lb_listener_rule" "S3_image_rule" {
  listener_arn = aws_lb_listener.lb_listener.arn
  priority     = 100
  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_target_group_proxy.arn
  }
  condition {
    path_pattern {
      values = ["/s3_cached/*"]
    }
  }
}
resource "aws_security_group" "lb_sg" {
  description = "Allow HTTP inbound traffic"
  name        = "${local.prefix}-lb-sg"
  vpc_id      = aws_vpc.vpc.id
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = local.common_tags
}