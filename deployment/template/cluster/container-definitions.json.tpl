[
    {
        "name": "web-app",
        "image": "${webapp_image}",
        "essential": true,
        "memoryReservation": 256,
        "environment": [
            {"name": "API_URL", "value": "${api_url}"},
            {"name": "LB_URL", "value": "${lb_url}"}
        ],
        "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
                "awslogs-group": "${log_group_name}",
                "awslogs-region": "${log_group_region}",
                "awslogs-stream-prefix": "web-app"
            }
        },
        "portMappings": [
            {
                "containerPort": 80,
                "hostPort": 80
            }
        ],
        "mountPoints": [
        ]
    },
    {
        "name": "proxy",
        "image": "${proxy_image}",
        "essential": true,
        "portMappings": [
            {
                "containerPort": 8080,
                "hostPort": 8080
            }
        ],
        "memoryReservation": 256,
        "environment": [
            {"name": "S3_BUCKET_KEY", "value": "aws-terraform-deployment-react-redux-platform-image-bucket-s3.s3.amazonaws.com"},
            {"name": "S3_BUCKET", "value": "https://aws-terraform-deployment-react-redux-platform-image-bucket-s3.s3.amazonaws.com/"},
            {"name": "LISTEN_PORT", "value": "8080"}
        ],
        "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
                "awslogs-group": "${log_group_name}",
                "awslogs-region": "${log_group_region}",
                "awslogs-stream-prefix": "proxy"
            }
        },
        "mountPoints": [
            
        ]
    }
]
