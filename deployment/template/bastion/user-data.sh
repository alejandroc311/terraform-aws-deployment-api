#!/bin/bash
sudo yum update -y
sudo yum install -y amazon-linux-extras
sudo yum install -y mysql mysql-client
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo usermod -aG docker ec2-user