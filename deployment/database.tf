resource "aws_db_subnet_group" "db_subnet_group" {
  name = "${local.prefix}-db-subnet-group"
  subnet_ids = [
    aws_subnet.private_subnet_a.id,
    aws_subnet.private_subnet_b.id,
  ]
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-db-subnet-group"
    })
  )
}
resource "aws_security_group" "db_security_group" {
  name        = "${local.prefix}-db-security-group"
  description = "Security group for the database"
  vpc_id      = aws_vpc.vpc.id
  ingress {
    from_port = 3306
    to_port   = 3306
    security_groups = [
      aws_security_group.bastion_sg.id,
      aws_security_group.ecs_cluster_security_group.id,
      aws_security_group.lambda_api_sg.id,
    ]
    protocol = "tcp"
  }
  ingress {
    from_port = 22
    to_port   = 22
    security_groups = [
      aws_security_group.bastion_sg.id,
    ]
    protocol = "tcp"
  }
  tags = local.common_tags
}
resource "aws_db_instance" "db_instance" {
  identifier                 = "${local.prefix}-db-instance"
  engine                     = "mysql"
  engine_version             = "5.7"
  instance_class             = "db.t2.micro"
  db_name                    = var.db_name
  username                   = var.db_user
  password                   = var.db_password
  allocated_storage          = 20
  storage_type               = "gp2"
  multi_az                   = false
  backup_retention_period    = 0
  auto_minor_version_upgrade = true
  vpc_security_group_ids = [
    aws_security_group.db_security_group.id,
  ]
  db_subnet_group_name = aws_db_subnet_group.db_subnet_group.name
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-db-instance"
    })
  )
}