resource "aws_vpc" "vpc" {
  cidr_block           = "10.1.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-vpc"
    })
  )
}
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-igw"
    })
  )
}
############################################################################################################
# Public Subnets #
############################################################################################################
resource "aws_subnet" "public_subnet_a" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "10.1.1.0/24"
  availability_zone       = "${data.aws_region.current.name}a"
  map_public_ip_on_launch = true
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-public-subnet-a"
    })
  )
}
resource "aws_route_table" "public_route_table_a" {
  vpc_id = aws_vpc.vpc.id
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-public-route-table-a"
    })
  )
}
resource "aws_route_table_association" "public_route_table_association_a" {
  subnet_id      = aws_subnet.public_subnet_a.id
  route_table_id = aws_route_table.public_route_table_a.id
}
resource "aws_route" "public_route_a" {
  route_table_id         = aws_route_table.public_route_table_a.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}
resource "aws_eip" "eip_a" {
  vpc = true
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-eip-a"
    })
  )
}
resource "aws_nat_gateway" "nat_gateway_a" {
  allocation_id = aws_eip.eip_a.id
  subnet_id     = aws_subnet.public_subnet_a.id
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-nat-gateway-a"
    })
  )
}
resource "aws_subnet" "public_subnet_b" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "10.1.2.0/24"
  availability_zone       = "${data.aws_region.current.name}b"
  map_public_ip_on_launch = true
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-public-subnet-b"
    })
  )
}
resource "aws_route_table" "public_route_table_b" {
  vpc_id = aws_vpc.vpc.id
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-public-route-table-b"
    })
  )
}
resource "aws_route_table_association" "public_route_table_association_b" {
  subnet_id      = aws_subnet.public_subnet_b.id
  route_table_id = aws_route_table.public_route_table_b.id
}
resource "aws_route" "public_route_b" {
  route_table_id         = aws_route_table.public_route_table_b.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}
resource "aws_eip" "eip_b" {
  vpc = true
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-eip-b"
    })
  )
}
resource "aws_nat_gateway" "nat_gateway_b" {
  allocation_id = aws_eip.eip_b.id
  subnet_id     = aws_subnet.public_subnet_b.id
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-nat-gateway-b"
    })
  )
}
############################################################################################################
# Private Subnets #
############################################################################################################

resource "aws_subnet" "private_subnet_a" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = "10.1.10.0/24"
  availability_zone = "${data.aws_region.current.name}a"
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-private-subnet-a"
    })
  )
}
resource "aws_route_table" "private_route_table_a" {
  vpc_id = aws_vpc.vpc.id
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-private-route-table-a"
    })
  )
}
resource "aws_route_table_association" "private_route_table_association_a" {
  subnet_id      = aws_subnet.private_subnet_a.id
  route_table_id = aws_route_table.private_route_table_a.id
}
resource "aws_route" "private_route_a" {
  route_table_id         = aws_route_table.private_route_table_a.id
  nat_gateway_id         = aws_nat_gateway.nat_gateway_a.id
  destination_cidr_block = "0.0.0.0/0"

}
resource "aws_subnet" "private_subnet_b" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = "10.1.11.0/24"
  availability_zone = "${data.aws_region.current.name}b"
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-private-subnet-b"
    })
  )
}
resource "aws_route_table" "private_route_table_b" {
  vpc_id = aws_vpc.vpc.id
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-private-route-table-b"
    })
  )
}
resource "aws_route_table_association" "private_route_table_association_b" {
  subnet_id      = aws_subnet.private_subnet_b.id
  route_table_id = aws_route_table.private_route_table_b.id
}
resource "aws_route" "private_route_b" {
  route_table_id         = aws_route_table.private_route_table_b.id
  nat_gateway_id         = aws_nat_gateway.nat_gateway_b.id
  destination_cidr_block = "0.0.0.0/0"
}
resource "aws_vpc_endpoint" "s3_endpoint" {
  vpc_id            = aws_vpc.vpc.id
  service_name      = "com.amazonaws.us-east-1.s3"
  vpc_endpoint_type = "Gateway"
  route_table_ids   = [aws_route_table.public_route_table_a.id, aws_route_table.public_route_table_b.id, aws_route_table.private_route_table_a.id, aws_route_table.private_route_table_b.id]

}
