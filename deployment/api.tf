resource "aws_security_group" "lambda_api_sg" {
  name        = "${local.prefix}-lambda-api-security-group"
  description = "Security group for the Lambda function API"
  vpc_id      = aws_vpc.vpc.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags
}
