variable "api_prefix" {
  default = "tapd"
}
variable "project" {
  default = "terraform-aws-platform-deployment"
}
variable "contact" {
  default = "alejandroc311@gmail.com"
}
variable "db_user" {
  description = "The database username"
  default     = "tapduser"
}
variable "db_password" {
  description = "The database password"
  default     = "tapdpassword"
}
variable "db_name" {
  description = "The database name"
  default     = "tapddbname"
}
variable "bastion_key_name" {
  default     = "terraform-aws-platform-deployment-bastion"
  description = "The name of the key pair to use for the bastion host"
}
variable "ecr_image_webapp" {
  description = "The ECR image to use for the webapp/react app."
  default     = "853006830105.dkr.ecr.us-east-1.amazonaws.com/terraform-aws-deployment-app:latest"
}
variable "ecr_image_proxy" {
  description = "The ECR image to use for the proxy."
  default     = "853006830105.dkr.ecr.us-east-1.amazonaws.com/terraform-aws-deployment-platform-api-proxy:latest"
}
variable "s3_image_bucket" {
  description = "The S3 bucket to use for the users' images."
  default     = "aws-terraform-deployment-react-redux-platform-image-bucket-s3.s3.amazonaws.com"
}
variable "api_url" {
  description = "The API URL that the webapp will use."
  default     = "/api"
}
variable "lb_host" {
  description = "The host for the load balancer."
  default     = "/lb"
}