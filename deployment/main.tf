terraform {
  backend "s3" {
    bucket         = "aws-terraform-platform-deployment-api-tf-state"
    key            = "platform-api.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "aws-terraform-platform-deployment-api-tf-state-lock"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.54.0"
    }
  }
}
provider "aws" {
  region = "us-east-1"
}

locals {
  prefix = "${var.api_prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
data "aws_region" "current" {}