output "db_host" {
  value = aws_db_instance.db_instance.address
}
output "bastion_host" {
  value = aws_instance.bastion.public_dns
}
output "lb_endpoint" {
  value = aws_lb.lb.dns_name
}
output "lambda_sg_id" {
  value = aws_security_group.lambda_api_sg.id
}
output "public_subnet_id_a" {
  value = aws_subnet.public_subnet_a.id
}
output "public_subnet_id_b" {
  value = aws_subnet.public_subnet_b.id
}
output "private_subnet_id_a" {
  value = aws_subnet.private_subnet_a.id
}
output "private_subnet_id_b" {
  value = aws_subnet.private_subnet_b.id
}