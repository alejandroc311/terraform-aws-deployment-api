CREATE TABLE `mockups` (
    `id` int NOT NULL AUTO_INCREMENT,
    `proyectId` int DEFAULT NULL,
    `src` varchar(255) DEFAULT NULL,
    `rating` int DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `proyectId` (`proyectId`)
);

CREATE TABLE `proyects` (
    `id` int NOT NULL AUTO_INCREMENT,
    `userId` int DEFAULT NULL,
    `adminId` int DEFAULT NULL,
    `lastModified` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `userId` (`userId`)
);

CREATE TABLE `comments` (
    `id` int NOT NULL AUTO_INCREMENT,
    `proyectId` int NOT NULL,
    `dateCreated` datetime NOT NULL,
    `comment` text,
    PRIMARY KEY (`id`)
);

CREATE TABLE `accounts` (
    `id` int NOT NULL AUTO_INCREMENT,
    `userId` int NOT NULL,
    PRIMARY KEY (`id`),
    KEY `userId` (`userId`)
);

CREATE TABLE `users` ( 
    `id` int NOT NULL AUTO_INCREMENT, 
    `email` varchar(255) DEFAULT NULL, 
    `hashedPassword` varchar(255) DEFAULT NULL, 
    `proyectId` int DEFAULT NULL, 
    `accountId` int DEFAULT NULL, 
    PRIMARY KEY (`id`), 
    KEY `proyectId` (`proyectId`), 
    KEY `accountId` (`accountId`) 
);

CREATE TABLE admins(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL,
    hashedPassword VARCHAR(255) NOT NULL
);

DELIMITER $$
CREATE PROCEDURE `SP_CREATE_USER` (
    IN emailInput VARCHAR(255),
    IN passwordInput VARCHAR(255),
    OUT dirNum INT
)
BEGIN 
    DECLARE EXIT HANDLER FOR SQLEXCEPTION 
    BEGIN 
    ROLLBACK;
    RESIGNAL;
    END;
    START TRANSACTION;
    SET @adminid = (SELECT id FROM admins ORDER BY RAND() LIMIT 1); 
    INSERT INTO users(email, hashedPassword) VALUES (emailInput, passwordInput);
    SET @userid = LAST_INSERT_ID();
    INSERT INTO accounts(userId) VALUES (@userid);
    SET @accountid = LAST_INSERT_ID();
    INSERT INTO proyects(userId, adminId, lastModified) VALUES (@userid, @adminid, NOW());
    SET @proyectid = LAST_INSERT_ID();
    UPDATE users SET accountId = @accountid, proyectId = @proyectid WHERE id = @userid;
    SET dirNum = @proyectid;
    COMMIT WORK; 

END$$
DELIMITER ;
