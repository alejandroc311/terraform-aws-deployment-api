# About
This is an API server available on `https://gitlab.com/alejandroc311/terraform-aws-deployment-api`. This API server is meant to work in tandem with the UI available over at `https://gitlab.com/alejandroc311/terraform-aws-deployment`.

# Stack
This API server uses the `Express` and `Serverless` frameworks in order to leverage the effectiveness and simplicity of routing with `Express` along with the cost benefits of Lambda Functions as opposed to dedicated servers. Furthermore, this API is containerized using `Docker` and the resulting image is pushed up to `AWS ECR` via a `Gitlab CI/CD pipeline.`

# Run Locally
There are two (2) options when it comes to running the API locally; Either the Serverless CLI or the Docker CLI.
1. Docker CLI 
    - First, make sure you have Docker properly installed in your system.
    - Then, run `docker-compose up api -d` to run the API locally from a Docker Container
    - The `api` will be running on `http://localhost:3000`.
2. Serverless CLI
    - First, install the serverless framework by running `npm install -g serverless`.
    - Then, add the `serverless-offline` plugin by running `serverless plugin install -n serverless-offline`
    - Finally, run `serverless offline` and the `api` will be running on `http://localhost:3000`. 
