import express, { NextFunction, Request, Response } from "express";
import serverless from "serverless-http";
import cors from "cors";
import mysql from "mysql2";
import jwt from "jsonwebtoken";
import fileUpload from "express-fileupload";
import aws from "aws-sdk";
const app = express();
const corsOptions = {
  origin: "replace_me_lb_endpoint",
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  credentials: true,
  optionsSuccessStatus: 204,
};
app.use(cors(corsOptions));
app.use(express.json());
app.use(fileUpload());
app.options("*", cors());
process.on("unhandledRejection", (err) => {
  console.error("Unhandled Promise Rejection:", err);
});

const s3 = new aws.S3();

const connection = mysql.createPool({
  host: "replace_me_db_host",
  user: "replace_me_db_user",
  password: "replace_me_db_password",
  multipleStatements: true,
  database: "replace_me_db_name",
  port: 3306,
});
const connect = () =>
  new Promise((resolve, reject) => {
    try {
      connection.execute("SHOW DATABASES", [], (error: any, results: any) => {
        if (error) throw error;
        resolve(results);
      });
    } catch (error) {
      console.error(error);
      reject(error);
    }
  });

const checkForUser = (email: any) =>
  new Promise((resolve, reject) => {
    try {
      connection.execute(
        "SELECT `email` FROM `users` WHERE `email` = ?",
        [email],
        (error: any, results: any) => {
          if (error) throw error;
          results.length > 0
            ? reject(new Error("User already exists."))
            : resolve(false);
        }
      );
    } catch (error) {
      console.error("Error on DB connection", error);
      reject(error);
    }
  });
const getAdmin = (email: any) =>
  new Promise((resolve, reject) => {
    try {
      connection.execute(
        "SELECT * FROM `admins` WHERE `email` = ?",
        [email],
        (error: any, results: any) => {
          if (error) throw error;
          results.length > 0 ? resolve(results) : reject(results);
        }
      );
    } catch (error) {
      console.error("Error on DB connection: ", error);
      reject(error);
    }
  });
const getAdminProyects = (adminId: any) =>
  new Promise((resolve, reject) => {
    try {
      connection.execute(
        " SELECT proyects.id, proyects.lastModified, proyects.userId, users.email as userEmail, admins.email as adminEmail FROM proyects INNER JOIN users on proyects.userId=users.id INNER JOIN admins on proyects.adminId=?",
        [adminId],
        (error: any, results: any) => {
          if (error) throw error;
          results.length > 0
            ? resolve(results)
            : reject(new Error("No proyects for this Admin"));
        }
      );
    } catch (error) {
      console.error("Error on DB connection: ", error);
      reject(error);
    }
  });
const getComments = (proyectId: any) =>
  new Promise((resolve, reject) => {
    try {
      connection.execute(
        "SELECT * FROM comments where proyectId=?",
        [proyectId],
        (err: any, results: any) => {
          if (err) throw err;
          results.length > 0
            ? resolve(results)
            : reject(new Error("No comments yet..."));
        }
      );
    } catch (error) {
      console.error(error);
      reject(error);
    }
  });
const getMockups = (proyectId: any) =>
  new Promise((resolve, reject) => {
    let mockups;
    try {
      connection.execute(
        "SELECT * FROM `mockups` WHERE `proyectId` = ?",
        [proyectId],
        (error: any, results: any) => {
          if (error) throw error;
          if (results.length > 0) {
            mockups = [...results];
            resolve(mockups);
          } else {
            reject(new Error("No Mockups for this Proyect yet ..."));
          }
        }
      );
    } catch (error: any) {
      console.error("Error on DB Connection", error);
      reject(new Error("Error on DB"));
    }
  });
const getUser = (email: any) =>
  new Promise((resolve, reject) => {
    try {
      connection.execute(
        "SELECT * FROM `users` WHERE `email` = ?",
        [email],
        (error: any, results: any) => {
          if (error) throw error;
          results.length > 0
            ? resolve(results)
            : reject(new Error("Error on Authentication"));
        }
      );
    } catch (error) {
      console.error("Error on DB connection", error);
      reject(error);
    }
  });
const uploadMockups = (mockupsValues, proyectId) =>
  new Promise((resolve, reject) => {
    try {
      const placeholders = mockupsValues.map(() => "(?, ?)").join(", ");
      const flattenedValues = [].concat(...mockupsValues);
      connection.execute(
        `INSERT INTO mockups(proyectId, src) VALUES ${placeholders}`,
        flattenedValues,
        (err: any) => {
          if (err) throw err;
          connection.execute(
            `UPDATE proyects SET lastModified = NOW() WHERE id = ?`,
            [proyectId],
            (err: any) => {
              if (err) throw err;
              resolve("Successfully inserted mockup path and id.");
            }
          );
        }
      );
    } catch (error) {
      console.error(error);
      reject(new Error("Problem uploading mockup path and id."));
    }
  });

const setMockupRating = (score, id, proyectId) =>
  new Promise((resolve, reject) => {
    try {
      connection.execute(
        "UPDATE mockups SET rating = ? WHERE id = ?",
        [score, id],
        (error: any) => {
          if (error) throw error;
          connection.execute(
            "UPDATE proyects SET lastModified = NOW() WHERE id = ?",
            [proyectId],
            (error: any) => {
              if (error) throw error;
              resolve("Successfully set mockup rating.");
            }
          );
        }
      );
    } catch (error) {
      console.error(error);
      reject(new Error("Error setting up comment."));
    }
  });

const setProyectComment = (proyectId, comment) =>
  new Promise((resolve, reject) => {
    try {
      connection.execute(
        "INSERT INTO comments(proyectId, dateCreated, comment) VALUES(?, NOW(), ?);",
        [proyectId, comment],
        (error: any) => {
          if (error) throw error;

          // Execute the second statement
          connection.execute(
            "UPDATE proyects SET lastModified = NOW() WHERE id = ?;",
            [proyectId],
            (error: any) => {
              if (error) throw error;
              resolve("Successfully set up comment.");
            }
          );
        }
      );
    } catch (error) {
      console.error(error);
      reject(new Error("Error setting comment"));
    }
  });

const createUser = (email, password) =>
  new Promise((resolve, reject) => {
    try {
      connection.execute(
        "CALL SP_CREATE_USER(?, ?, @return);",
        [email, password],
        (error: any, results: any) => {
          if (error) throw error;
          console.log(results);
          resolve("Successfully created user.");
        }
      );
    } catch (error) {
      console.error(error);
      reject(error);
    }
  });
app.post("/authenticateAdmin", async (req: any, res: any, next: any) => {
  let token, proyects, id;
  try {
    [, token] = req.headers.authorization.split(" ");
    jwt.verify(token, "secret", (err: any, decoded: any) => {
      if (err) throw err;
      ({ id } = decoded);
    });
    proyects = await getAdminProyects(id);
    res.json({
      body: {
        admin: {
          id,
          proyects,
        },
      },
    });
  } catch (error) {
    console.error(error);
    next(error);
  }
});
app.post("/authenticateUser", async (req: any, res: any, next: any) => {
  let token, user, email;
  try {
    [, token] = req.headers.authorization.split(" ");
    jwt.verify(token, "secret", (err: any, decoded: any) => {
      if (err) throw err;
      ({ email } = decoded);
    });
    [user] = (await getUser(email)) as any;
    const { id, accountId, proyectId } = user;
    res.json({
      body: {
        id,
        accountId,
        proyectId,
      },
    });
  } catch (error) {
    console.error(error);
    next(error);
  }
});
app.post("/createUser", async (req: any, res: any, next: any) => {
  const {
    body: { email, password },
  } = req;
  console.log(email, password);
  let doesUserExist;
  try {
    doesUserExist = await checkForUser(email);
    if (doesUserExist === false) {
      const createdUser = await createUser(email, password);
      console.log(createdUser);
      res.status(200).json({
        Success: "User Created",
      });
    }
  } catch (error) {
    console.error(error);
    next(error);
  }
});
app.post("/getComments", async (req: any, res: any, next: any) => {
  let comments;
  const {
    body: { proyectId },
  } = req;
  const [, token] = req.headers.authorization.split(" ");
  try {
    jwt.verify(token, "secret", (err: any) => {
      if (err) throw err;
    });
    comments = await getComments(proyectId);
    console.log(comments);
    res.json({
      body: {
        comments,
      },
    });
  } catch (error) {
    console.error(error);
    next(error);
  }
});
app.post("/getMockups", async (req: any, res: any, next: any) => {
  const {
    body: { proyectId },
  } = req;
  const [, token] = req.headers.authorization.split(" ");
  let mockups;
  try {
    jwt.verify(token, "secret", (err: any) => {
      if (err) throw err;
    });
    mockups = await getMockups(proyectId);
    res.status(200).json({
      mockups,
    });
  } catch (error) {
    console.error(error);
    next(error);
  }
});
app.post("/loginAdmin", async (req: any, res: any, next: any) => {
  const {
    body: { email, password },
  } = req;
  let admin, proyects, accessToken;
  try {
    [admin] = (await getAdmin(email)) as any;
    const { id, hashedPassword } = admin;
    if (hashedPassword === password) {
      proyects = await getAdminProyects(id);
      console.table(proyects);
      accessToken = jwt.sign(
        {
          id,
        },
        "secret",
        { expiresIn: 60 * 60 }
      );
      res.json({
        body: {
          admin: {
            id,
            proyects,
          },
          accessToken,
        },
      });
    } else {
      throw new Error("Error on Login");
    }
  } catch (error) {
    console.error(error);
    next(error);
  }
});
app.post("/loginUser", async (req: any, res: any, next: any) => {
  const {
    body: { email, password },
  } = req;
  let user: any;
  try {
    [user] = (await getUser(email)) as any;
    const { hashedPassword } = user;
    if (password !== hashedPassword) throw new Error("Error on Authentication");
    const { id, proyectId, accountId } = user;
    const accessToken = jwt.sign(
      {
        accountId,
        id,
        email,
        proyectId,
      },
      "secret",
      { expiresIn: 60 * 60 }
    );
    res.status(200).json({
      body: {
        user,
        accessToken,
      },
    });
  } catch (error) {
    console.error("Error on Login: ", error);
    next(error);
  }
});

app.post("/setMockupRating", async (req: any, res: any, next: any) => {
  let {
    body: { id, score, proyectId },
  } = req;
  score === "" ? (score = 0) : score;
  const [, token] = req.headers.authorization.split(" ");
  try {
    jwt.verify(token, "secret", (err: any) => {
      if (err) throw err;
    });
    const setRating = await setMockupRating(score, id, proyectId);
    console.log(setRating);
    res.json({
      result: "success",
    });
  } catch (error) {
    console.error(error);
    next(error);
  }
});

app.post("/setProyectComment", async (req: any, res: any, next: any) => {
  let {
    body: { proyectId, comment },
  } = req;
  console.log(proyectId, comment);
  const [, token] = req.headers.authorization.split(" ");
  try {
    jwt.verify(token, "secret", (err: any) => {
      if (err) throw err;
    });
    const setComment = await setProyectComment(proyectId, comment);
    console.log(setComment);
    res.json({
      result: "success",
    });
  } catch (error) {
    console.error(error);
    next(error);
  }
});
app.post("/uploadMockups", async (req: any, res: any, next: any) => {
  let uploadPath: any,
    fileNames: any[] = [];
  try {
    const {
      body: { proyectId },
    } = req;
    const [, token] = req.headers.authorization.split(" ");
    jwt.verify(token, "secret", (err: any) => {
      if (err) throw err;
    });

    console.log({ req });
    for (const [, value] of Object.entries(req.files) as [value: any]) {
      try {
        console.log("File about to start processing:", value);
        uploadPath = `s3_cached/${proyectId}/${value.name}`;
        fileNames = [...fileNames, value.name];
        console.log("Mockups about to start upload to bucket:", fileNames);
        const params = {
          Bucket:
            "aws-terraform-deployment-react-redux-platform-image-bucket-s3",
          Key: uploadPath,
          Body: value.data,
        };

        const putObjectPromise = new Promise((resolve, reject) => {
          console.log("Inside promise ...");
          s3.putObject(params, function (err, data) {
            console.log("Inside putObject callback");
            if (err) {
              console.error("S3 putObject error for file:", value.name, err);
              reject(err);
            } else resolve(data);
          });
        });

        const result = await putObjectPromise;
        console.log("Upload result for file:", value.name, result);
      } catch (error) {
        console.error("Error processing file:", value.name, error);
        next(error);
      }
    }

    console.log("About to start parsing to commit to db:");
    const mockupsValues = fileNames.map((fileName) => {
      const src = `https://aws-terraform-deployment-react-redux-platform-image-bucket-s3.s3.amazonaws.com/s3_cached/${proyectId}/${fileName}`;
      return [proyectId, src];
    });
    const mockupUploadResult = await uploadMockups(mockupsValues, proyectId);
    console.log("Finished parsing to commit to db:", mockupUploadResult);

    res.json({
      result: "success",
    });
  } catch (error) {
    console.error(error);
    next(error);
  }
});

app.get("/", async (req: Request, res: Response, next: NextFunction) => {
  const connectTrial = await connect();
  console.log(connectTrial);
  return res.status(200).json({
    message: `Request came from ${req.ip}`,
  });
});
module.exports.handler = serverless(app);
